## T89571AA3-user 11 RKQ1.200928.002 00WW_3_150 release-keys
- Manufacturer: hmd global
- Platform: msm8937
- Codename: PAN_sprout
- Brand: Nokia
- Flavor: T89571AA3-user
- Release Version: 11
- Id: RKQ1.200928.002
- Incremental: 00WW_3_150
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 300
- Fingerprint: Nokia/Panther_00WW/PAN_sprout:11/RKQ1.200928.002/00WW_3_150:user/release-keys
- OTA version: 
- Branch: T89571AA3-user-11-RKQ1.200928.002-00WW_3_150-release-keys
- Repo: nokia_pan_sprout_dump_30226


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
